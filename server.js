const express = require('express');
const port = process.env.PORT || 3000;

const app = express();

app.get('/',(req,res) => {
    res.send('Hello from using OAuth with node js');
});

app.listen(port,() => {
    console.log(`Server is up and running on port : ${port}`);
});